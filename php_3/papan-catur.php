<?php

function papan_catur($angka) {
 for ($i=0; $i < $angka; $i++) { 
   for ($j=0; $j < $angka; $j++) { 
     if ($i % 2) {
       echo '#';
       echo ' ';
     } else {
       echo '<br>';
       echo ' ';
       echo '#';
     }
   }
 }
};

// TEST CASES
echo papan_catur(4);
/*
# # # #
 # # #
# # # #
 # # #
 */

echo papan_catur(8);
/* 
# # # # # # # #
 # # # # # # # 
# # # # # # # #
 # # # # # # # 
# # # # # # # #
 # # # # # # #
# # # # # # # #
 # # # # # # #

echo papan_catur(5) 
/*
# # # # #
 # # # #
# # # # #
 # # # # 
# # # # #
*/
?>