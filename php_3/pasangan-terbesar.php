<?php
function pasangan_terbesar($angka){
  $pasangan = '';
  $pasangan_pertama = '';
  $pasangan_selanjutnya = '';
  for ($i=0; $i < strlen($angka); $i++) {
    if(strlen($pasangan_pertama) > 2) {
      $pasangan_selanjutnya .= $angka[$i];
      if($pasangan_pertama > $pasangan_selanjutnya) {
        $pasangan .= $pasangan_pertama;
      } else {
        $pasangan .= $pasangan_selanjutnya;
      }
    } else {
      $pasangan_pertama .= $angka[$i];
    }
  }
  return $pasangan;
}

// TEST CASES
echo pasangan_terbesar(641573). '<br>'; // 73
echo pasangan_terbesar(12783456). '<br>'; // 83
echo pasangan_terbesar(910233). '<br>'; // 91
echo pasangan_terbesar(71856421). '<br>'; // 85
echo pasangan_terbesar(79918293). '<br>'; // 99

?>