<?php

function palindrome_angka($angka) {
  $hasil='';
 if($angka >= 0 && $angka <=9) {
   $hasil .= $angka +1;
 } else if(strlen($angka) == 2){
    $angka_pertama = substr($angka,0,1);
    $angka_kedua = substr($angka,1,1);
    if($angka_pertama == $angka_kedua) {
      $hasil = $angka;
    } else {
      if($angka_pertama > $angka_kedua) {
        $hasil = $angka_pertama . $angka_pertama;
      } else {
        $hasil = ($angka_pertama + 1) . ($angka_pertama + 1);
      }
    }
 } else if(strlen($angka) == 3){
    $angka_pertama = substr($angka,0,1);
    $angka_kedua = substr($angka,1,1);
    $angka_ketiga = substr($angka,2,1);
    if($angka_pertama == $angka_ketiga) {
      $hasil = $angka;
    } else {
      if($angka_pertama > $angka_ketiga) {
        $hasil = $angka_pertama . $angka_kedua .$angka_pertama;
      } else {
        $hasil = $angka_pertama . ($angka_kedua + 1) . $angka_pertama;
      }
    }
 } else if(strlen($angka) == 4){
    $angka_pertama = substr($angka,0,1);
    $angka_kedua = substr($angka,1,1);
    $angka_ketiga = substr($angka,2,1);
    $angka_keempat = substr($angka,3,1);
    if($angka_pertama == $angka_keempat && $angka_kedua == $angka_ketiga) {
      $hasil = $angka;
    } else {
      if($angka_pertama == $angka_keempat) {
        if($angka_kedua > $angka_ketiga ) {
          $hasil = $angka_pertama . $angka_kedua . $angka_kedua . $angka_pertama;
        } else {
          $hasil = $angka_pertama . $angka_ketiga . $angka_ketiga . $angka_pertama;
        }
      } else {
        if($angka_kedua == $angka_ketiga) {
          $hasil = $angka_pertama . $angka_kedua . $angka_ketiga . $angka_pertama;
        } else if($angka_kedua > $angka_ketiga ) {
          $hasil = $angka_pertama . $angka_kedua . $angka_kedua . $angka_pertama;
        } else if($angka_kedua < $angka_ketiga ) {
          $hasil = $angka_pertama . $angka_ketiga . $angka_ketiga . $angka_pertama;
        }
      }
    }
  }
 return $hasil;
};

// TEST CASES
echo palindrome_angka(8). "<br>"; // 9
echo palindrome_angka(10). "<br>"; // 11
echo palindrome_angka(117). "<br>"; // 121
echo palindrome_angka(175). "<br>"; // 181
echo palindrome_angka(1000). "<br>"; // 1001

?>