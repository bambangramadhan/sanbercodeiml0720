<?php
function ubah_huruf($string){
  $huruf_aneh='';
  $huruf = ['a','b','c','d','e','f','g',"h",'i',"j",'k','l',"m",'o',"p","q",'r',"s",'t','u','v','w','x','y','z'];
  for ($i=0; $i < strlen($huruf) - 1; $i++) { 
   $huruf_aneh .= $huruf[$i + 1];
  };
}

// TEST CASES
echo ubah_huruf('wow'). '<br>'; // xpx
echo ubah_huruf('developer'). '<br>'; // efwfmpqfs
echo ubah_huruf('laravel'). '<br>'; // mbsbwfm
echo ubah_huruf('keren'). '<br>'; // lfsfo
echo ubah_huruf('semangat'). '<br>'; // tfnbohbu

?>